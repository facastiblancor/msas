`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    08:51:05 04/14/2016 
// Design Name: 
// Module Name:    i2c_controller 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module i2c_controller (
    input  				clk,

    output 				i2c_sclk,
    inout  				i2c_sdat,

    input  				start,
	 input  				rw,
//	 input [6:0] 		slave_address,
	 output				led0,
	 output				led1,
	 output				led2,
	 output				led3,
    output 				done,
    output 				ack


);

reg [7:0] data;

reg [5:0] stage;
reg [10:0] sclk_divider;
reg clock_en;
reg [6:0] address;
reg [7:0] slave_register;
reg rep_start;

// don't toggle the clock unless we're sending data
// clock will also be kept high when sending START and STOP symbols
assign i2c_sclk = (!clock_en) || (sclk_divider >= 11'd512);
wire midlow = (sclk_divider >= 11'd453); //Para ajustar la subida de SDA antes de SCL


reg sdat = 1'b1;
// rely on pull-up resistor to set SDAT high
assign i2c_sdat = (sdat) ? 1'bz : 1'b0;

reg [3:0] acks;

parameter LAST_STAGE = 6'd40;

assign ack = (acks == 4'b1000);
assign done = (stage == LAST_STAGE);
assign {led3,led2,led1,led0} = data[3:0];

always @(posedge clk) begin

	 if(rep_start) 
			begin
				clock_en <= 1'b0;
				rep_start <= 1'b0;
			end else
          
    if (start) begin
        sclk_divider <= 11'd0;
        stage <= 5'd0;
        clock_en <= 1'b0;
        sdat <= 1'b1;
        acks <= 3'b111;
        data <= 8'b00100110;
		  address <= 7'b1101000;
		  slave_register <= 8'b00000001;
    end else begin
        if (sclk_divider == 11'd1024) begin
            sclk_divider <= 11'd0;

            if (stage != LAST_STAGE)
                stage <= stage + 1'b1;

            case (stage)
                // after start
                6'd0:  clock_en <= 1'b1;
                // receive acks
                6'd9:  acks[0] <= i2c_sdat;
                6'd18: acks[1] <= i2c_sdat;
					 6'd20: if(~rw) clock_en <= 1'b1;
					 6'd27: if(rw)  acks[2] <= i2c_sdat;
					 6'd29: if(~rw) acks[2] <= i2c_sdat;
					 6'd38: if(~rw) acks[3] <= i2c_sdat;

                // before stop
            endcase
        end else
            sclk_divider <= sclk_divider + 1'b1;

        if (midlow && (rw == 1)) begin //Lógica de escritura (rw = 1)
            case (stage)
                // start
                6'd0:  sdat <= 1'b0;
                // byte 1
                //5'd1:  sdat <= data[23];1101000 Logica de escritura (muy forzada, demasiado forzada)
					 6'd1:  sdat <= address[6];
                6'd2:  sdat <= address[5];
                6'd3:  sdat <= address[4];
                6'd4:  sdat <= address[3];
                6'd5:  sdat <= address[2];
                6'd6:  sdat <= address[1];
                6'd7:  sdat <= address[0];
                6'd8:  sdat <= 1'b0; //Bit de Lectura/Escritura
                // ack 1
                6'd9:  sdat <= acks[0];//Se asigna dos veces
                // byte 2
                6'd10: sdat <= slave_register[7];	//Envío de la dirección del registro del esclavo
                6'd11: sdat <= slave_register[6];
                6'd12: sdat <= slave_register[5];
                6'd13: sdat <= slave_register[4];
                6'd14: sdat <= slave_register[3];
                6'd15: sdat <= slave_register[2];
                6'd16: sdat <= slave_register[1];
                6'd17: sdat <= slave_register[0];
                // ack 2
                6'd18: sdat <= acks[1];
                // byte 3 Escritura de datos
                6'd19: sdat <= data[7];
                6'd20: sdat <= data[6];
                6'd21: sdat <= data[5];
                6'd22: sdat <= data[4];
                6'd23: sdat <= data[3];
                6'd24: sdat <= data[2];
                6'd25: sdat <= data[1];
                6'd26: sdat <= data[0];
                // ack 3
                6'd27: sdat <= acks[2];
                // stop
                6'd28: begin 
					 sdat <= 1'b0;
					 clock_en <= 1'b0;
					 end
                6'd29: sdat <= 1'b1;
            endcase
				end //yo coloqué esto
				if (midlow && (rw == 0)) begin //Lógica de lectura (rw = 0)
            case (stage)
                // start
                6'd0:  sdat <= 1'b0;
                // byte 1
                //5'd1:  sdat <= data[23];1101000 Logica de escritura (muy forzada, demasiado forzada)
					 6'd1:  sdat <= address[6];
                6'd2:  sdat <= address[5];
                6'd3:  sdat <= address[4];
                6'd4:  sdat <= address[3];
                6'd5:  sdat <= address[2];
                6'd6:  sdat <= address[1];
                6'd7:  sdat <= address[0];
                6'd8:  sdat <= 1'b0; //Bit de Lectura/Escritura
                // ack 1
                6'd9:  sdat <= acks[0];
                // byte 2
                6'd10: sdat <= slave_register[7];	//Envío de la dirección del registro del esclavo
                6'd11: sdat <= slave_register[6];
                6'd12: sdat <= slave_register[5];
                6'd13: sdat <= slave_register[4];
                6'd14: sdat <= slave_register[3];
                6'd15: sdat <= slave_register[2];
                6'd16: sdat <= slave_register[1];
                6'd17: sdat <= slave_register[0];
                // ack 2
                6'd18: sdat <= acks[1];
					 //Repetir condicion de inicio
					 6'd19: begin 
						 rep_start <= 1'b1;
						 sdat <= 1'b1;
					 end
					 6'd20:  sdat <= 1'b0;
					 // Volver a enviar la direccion del esclavo
                6'd21:  sdat <= address[6];
                6'd22:  sdat <= address[5];
                6'd23:  sdat <= address[4];
                6'd24:  sdat <= address[3];
                6'd25:  sdat <= address[2];
                6'd26:  sdat <= address[1];
                6'd27:  sdat <= address[0];
                6'd28:  sdat <= 1'b1;    // Confirmación de lectura
					 //ack3
                6'd29: sdat <= acks[2];
                // recepción de datos
					 6'd30: data[7] <= sdat;
					 6'd31: data[6] <= sdat;
					 6'd32: data[5] <= sdat;
					 6'd33: data[4] <= sdat;
					 6'd34: data[3] <= sdat;
					 6'd35: data[2] <= sdat;
					 6'd36: data[1] <= sdat;
					 6'd37: data[0] <= sdat;
					 //ack4
					 6'd38: sdat <= 1'b1;
					 //Finalizar la operación
                6'd39: begin 
					 sdat <= 1'b0;
					 clock_en <= 1'b0;
					 end
					 6'd40: sdat <= 1'b1;
            endcase
				end
        end
    end

endmodule
