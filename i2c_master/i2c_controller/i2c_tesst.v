`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   12:54:20 04/16/2016
// Design Name:   i2c_controller
// Module Name:   /home/andres/Documentos/Digital II/i2c_controller/i2c_tesst.v
// Project Name:  i2c_controller
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: i2c_controller
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module i2c_tesst;

	// Inputs
	reg clk;
	reg start;
	reg rw;
	reg [1:0] i2c_data;

	// Outputs
	wire i2c_sclk;
	wire done;
	wire ack;

	// Bidirs
	wire i2c_sdat;
	
	//Parameter
	parameter PERIOD = 10;
	parameter real DUTY_CYCLE = 0.5;
	parameter OFFSET = 0;
	// Instantiate the Unit Under Test (UUT)
	i2c_controller uut (
		.clk(clk), 
		.i2c_sclk(i2c_sclk), 
		.i2c_sdat(i2c_sdat), 
		.start(start), 
		.rw(rw),
//		.slave_address(slave_address),
		.done(done), 
		.ack(ack)
	);
	
	
	initial begin
		// Initialize Inputs
		clk = 0;
		start = 1;
		rw = 0;
//		slave_address = 7'b1101000;
		i2c_data = 0;

		// Wait 100 ns for global reset to finish
		#100;
		start=0;
        
		// Add stimulus here

	end
   
	initial begin
		
		
		
		#OFFSET;
		forever
			begin
			clk = 1'b0;
			#(PERIOD - (PERIOD*DUTY_CYCLE)) clk = 1'b1;
			#(PERIOD*DUTY_CYCLE);
			end
		end
   
endmodule

