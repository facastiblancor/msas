////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
////////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: P.20131013
//  \   \         Application: netgen
//  /   /         Filename: i2c_controller_synthesis.v
// /___/   /\     Timestamp: Fri Apr 22 12:22:52 2016
// \   \  /  \ 
//  \___\/\___\
//             
// Command	: -intstyle ise -insert_glbl true -w -dir netgen/synthesis -ofmt verilog -sim i2c_controller.ngc i2c_controller_synthesis.v 
// Device	: xc7a100t-1-csg324
// Input file	: i2c_controller.ngc
// Output file	: /home/andres/Documentos/Digital II/i2c_controller/netgen/synthesis/i2c_controller_synthesis.v
// # of Modules	: 1
// Design Name	: i2c_controller
// Xilinx        : /opt/Xilinx/14.7/ISE_DS/ISE/
//             
// Purpose:    
//     This verilog netlist is a verification model and uses simulation 
//     primitives which may not represent the true implementation of the 
//     device, however the netlist is functionally correct and should not 
//     be modified. This file cannot be synthesized and should only be used 
//     with supported simulation tools.
//             
// Reference:  
//     Command Line Tools User Guide, Chapter 23 and Synthesis and Simulation Design Guide, Chapter 6
//             
////////////////////////////////////////////////////////////////////////////////

`timescale 1 ns/1 ps

module i2c_controller (
  clk, start, rw, i2c_sclk, i2c_sdat, led0, led1, led2, led3, done, ack
);
  input clk;
  input start;
  input rw;
  output i2c_sclk;
  inout i2c_sdat;
  output led0;
  output led1;
  output led2;
  output led3;
  output done;
  output ack;
  wire clk_BUFGP_0;
  wire start_IBUF_1;
  wire rw_IBUF_2;
  wire sdat_4;
  wire rep_start_5;
  wire clock_en_21;
  wire _n0148;
  wire midlow_GND_1_o_AND_39_o;
  wire done_OBUF_25;
  wire \stage[5]_acks[3]_select_19_OUT<3>_26 ;
  wire \sclk_divider[10]_GND_1_o_equal_7_o ;
  wire i2c_sclk_OBUF_28;
  wire \clock_en_stage[5]_MUX_103_o ;
  wire \sdat_stage[5]_MUX_102_o ;
  wire ack_OBUF_31;
  wire led3_OBUF_32;
  wire _n0213_inv1;
  wire Reset_OR_DriverANDClockEnable;
  wire _n0180_inv;
  wire rep_start_inv;
  wire \Result<5>1 ;
  wire _n0180_inv1_66;
  wire \Mmux_sdat_stage[5]_MUX_102_o22_67 ;
  wire \stage[5]_acks[3]_select_19_OUT<1>2 ;
  wire \Mmux_clock_en_stage[5]_MUX_103_o121 ;
  wire \Mmux_clock_en_stage[5]_MUX_103_o1 ;
  wire \Mmux_clock_en_stage[5]_MUX_103_o11_71 ;
  wire \Mmux_clock_en_stage[5]_MUX_103_o12_72 ;
  wire \Mmux_sdat_stage[5]_MUX_102_o2 ;
  wire \Mmux_sdat_stage[5]_MUX_102_o21_74 ;
  wire \Mmux_sdat_stage[5]_MUX_102_o23_75 ;
  wire \Mmux_sdat_stage[5]_MUX_102_o24_76 ;
  wire \Mmux_sdat_stage[5]_MUX_102_o25_77 ;
  wire \Mmux_sdat_stage[5]_MUX_102_o26_78 ;
  wire \Mmux_sdat_stage[5]_MUX_102_o27_79 ;
  wire \Mmux_sdat_stage[5]_MUX_102_o28_80 ;
  wire N0;
  wire N2;
  wire N4;
  wire N6;
  wire _n0213_inv12_85;
  wire N8;
  wire rep_start_glue_set_97;
  wire \Mcount_sclk_divider_cy<1>_rt_98 ;
  wire \Mcount_sclk_divider_cy<2>_rt_99 ;
  wire \Mcount_sclk_divider_cy<3>_rt_100 ;
  wire \Mcount_sclk_divider_cy<4>_rt_101 ;
  wire \Mcount_sclk_divider_cy<5>_rt_102 ;
  wire \Mcount_sclk_divider_cy<6>_rt_103 ;
  wire \Mcount_sclk_divider_cy<7>_rt_104 ;
  wire \Mcount_sclk_divider_cy<8>_rt_105 ;
  wire \Mcount_sclk_divider_cy<9>_rt_106 ;
  wire \Mcount_sclk_divider_xor<10>_rt_107 ;
  wire N10;
  wire N12;
  wire N13;
  wire N15;
  wire N16;
  wire N18;
  wire N19;
  wire N20;
  wire N21;
  wire sclk_divider_1_rstpot_117;
  wire sclk_divider_2_rstpot_118;
  wire sclk_divider_3_rstpot_119;
  wire sclk_divider_6_rstpot_120;
  wire sclk_divider_4_rstpot_121;
  wire sclk_divider_5_rstpot_122;
  wire sclk_divider_7_rstpot_123;
  wire sclk_divider_8_rstpot_124;
  wire sclk_divider_9_rstpot_125;
  wire sclk_divider_0_rstpot_126;
  wire sclk_divider_10_rstpot_127;
  wire stage_0_dpot_128;
  wire stage_1_dpot_129;
  wire stage_2_dpot_130;
  wire stage_3_dpot_131;
  wire stage_4_dpot_132;
  wire stage_5_dpot_133;
  wire _n01671_rstpot;
  wire acks_0_dpot_135;
  wire acks_1_dpot_136;
  wire acks_2_dpot_137;
  wire acks_3_dpot_138;
  wire [3 : 3] address;
  wire [3 : 0] acks;
  wire [10 : 0] sclk_divider;
  wire [10 : 0] Result;
  wire [5 : 0] stage;
  wire [0 : 0] Mcount_sclk_divider_lut;
  wire [9 : 0] Mcount_sclk_divider_cy;
  VCC   XST_VCC (
    .P(address[3])
  );
  GND   XST_GND (
    .G(led3_OBUF_32)
  );
  FDSE   acks_0 (
    .C(clk_BUFGP_0),
    .CE(\sclk_divider[10]_GND_1_o_equal_7_o ),
    .D(acks_0_dpot_135),
    .S(_n0148),
    .Q(acks[0])
  );
  FDSE   acks_1 (
    .C(clk_BUFGP_0),
    .CE(\sclk_divider[10]_GND_1_o_equal_7_o ),
    .D(acks_1_dpot_136),
    .S(_n0148),
    .Q(acks[1])
  );
  FDSE   acks_2 (
    .C(clk_BUFGP_0),
    .CE(\sclk_divider[10]_GND_1_o_equal_7_o ),
    .D(acks_2_dpot_137),
    .S(_n0148),
    .Q(acks[2])
  );
  FDRE   acks_3 (
    .C(clk_BUFGP_0),
    .CE(\sclk_divider[10]_GND_1_o_equal_7_o ),
    .D(acks_3_dpot_138),
    .R(_n0148),
    .Q(acks[3])
  );
  FDRE   clock_en (
    .C(clk_BUFGP_0),
    .CE(_n0213_inv1),
    .D(\clock_en_stage[5]_MUX_103_o ),
    .R(Reset_OR_DriverANDClockEnable),
    .Q(clock_en_21)
  );
  FDSE #(
    .INIT ( 1'b1 ))
  sdat (
    .C(clk_BUFGP_0),
    .CE(_n0180_inv),
    .D(\sdat_stage[5]_MUX_102_o ),
    .S(_n0148),
    .Q(sdat_4)
  );
  FDRE   stage_0 (
    .C(clk_BUFGP_0),
    .CE(\sclk_divider[10]_GND_1_o_equal_7_o ),
    .D(stage_0_dpot_128),
    .R(_n0148),
    .Q(stage[0])
  );
  FDRE   stage_1 (
    .C(clk_BUFGP_0),
    .CE(\sclk_divider[10]_GND_1_o_equal_7_o ),
    .D(stage_1_dpot_129),
    .R(_n0148),
    .Q(stage[1])
  );
  FDRE   stage_2 (
    .C(clk_BUFGP_0),
    .CE(\sclk_divider[10]_GND_1_o_equal_7_o ),
    .D(stage_2_dpot_130),
    .R(_n0148),
    .Q(stage[2])
  );
  FDRE   stage_3 (
    .C(clk_BUFGP_0),
    .CE(\sclk_divider[10]_GND_1_o_equal_7_o ),
    .D(stage_3_dpot_131),
    .R(_n0148),
    .Q(stage[3])
  );
  FDRE   stage_4 (
    .C(clk_BUFGP_0),
    .CE(\sclk_divider[10]_GND_1_o_equal_7_o ),
    .D(stage_4_dpot_132),
    .R(_n0148),
    .Q(stage[4])
  );
  FDRE   stage_5 (
    .C(clk_BUFGP_0),
    .CE(\sclk_divider[10]_GND_1_o_equal_7_o ),
    .D(stage_5_dpot_133),
    .R(_n0148),
    .Q(stage[5])
  );
  MUXCY   \Mcount_sclk_divider_cy<0>  (
    .CI(led3_OBUF_32),
    .DI(address[3]),
    .S(Mcount_sclk_divider_lut[0]),
    .O(Mcount_sclk_divider_cy[0])
  );
  XORCY   \Mcount_sclk_divider_xor<0>  (
    .CI(led3_OBUF_32),
    .LI(Mcount_sclk_divider_lut[0]),
    .O(Result[0])
  );
  MUXCY   \Mcount_sclk_divider_cy<1>  (
    .CI(Mcount_sclk_divider_cy[0]),
    .DI(led3_OBUF_32),
    .S(\Mcount_sclk_divider_cy<1>_rt_98 ),
    .O(Mcount_sclk_divider_cy[1])
  );
  XORCY   \Mcount_sclk_divider_xor<1>  (
    .CI(Mcount_sclk_divider_cy[0]),
    .LI(\Mcount_sclk_divider_cy<1>_rt_98 ),
    .O(Result[1])
  );
  MUXCY   \Mcount_sclk_divider_cy<2>  (
    .CI(Mcount_sclk_divider_cy[1]),
    .DI(led3_OBUF_32),
    .S(\Mcount_sclk_divider_cy<2>_rt_99 ),
    .O(Mcount_sclk_divider_cy[2])
  );
  XORCY   \Mcount_sclk_divider_xor<2>  (
    .CI(Mcount_sclk_divider_cy[1]),
    .LI(\Mcount_sclk_divider_cy<2>_rt_99 ),
    .O(Result[2])
  );
  MUXCY   \Mcount_sclk_divider_cy<3>  (
    .CI(Mcount_sclk_divider_cy[2]),
    .DI(led3_OBUF_32),
    .S(\Mcount_sclk_divider_cy<3>_rt_100 ),
    .O(Mcount_sclk_divider_cy[3])
  );
  XORCY   \Mcount_sclk_divider_xor<3>  (
    .CI(Mcount_sclk_divider_cy[2]),
    .LI(\Mcount_sclk_divider_cy<3>_rt_100 ),
    .O(Result[3])
  );
  MUXCY   \Mcount_sclk_divider_cy<4>  (
    .CI(Mcount_sclk_divider_cy[3]),
    .DI(led3_OBUF_32),
    .S(\Mcount_sclk_divider_cy<4>_rt_101 ),
    .O(Mcount_sclk_divider_cy[4])
  );
  XORCY   \Mcount_sclk_divider_xor<4>  (
    .CI(Mcount_sclk_divider_cy[3]),
    .LI(\Mcount_sclk_divider_cy<4>_rt_101 ),
    .O(Result[4])
  );
  MUXCY   \Mcount_sclk_divider_cy<5>  (
    .CI(Mcount_sclk_divider_cy[4]),
    .DI(led3_OBUF_32),
    .S(\Mcount_sclk_divider_cy<5>_rt_102 ),
    .O(Mcount_sclk_divider_cy[5])
  );
  XORCY   \Mcount_sclk_divider_xor<5>  (
    .CI(Mcount_sclk_divider_cy[4]),
    .LI(\Mcount_sclk_divider_cy<5>_rt_102 ),
    .O(Result[5])
  );
  MUXCY   \Mcount_sclk_divider_cy<6>  (
    .CI(Mcount_sclk_divider_cy[5]),
    .DI(led3_OBUF_32),
    .S(\Mcount_sclk_divider_cy<6>_rt_103 ),
    .O(Mcount_sclk_divider_cy[6])
  );
  XORCY   \Mcount_sclk_divider_xor<6>  (
    .CI(Mcount_sclk_divider_cy[5]),
    .LI(\Mcount_sclk_divider_cy<6>_rt_103 ),
    .O(Result[6])
  );
  MUXCY   \Mcount_sclk_divider_cy<7>  (
    .CI(Mcount_sclk_divider_cy[6]),
    .DI(led3_OBUF_32),
    .S(\Mcount_sclk_divider_cy<7>_rt_104 ),
    .O(Mcount_sclk_divider_cy[7])
  );
  XORCY   \Mcount_sclk_divider_xor<7>  (
    .CI(Mcount_sclk_divider_cy[6]),
    .LI(\Mcount_sclk_divider_cy<7>_rt_104 ),
    .O(Result[7])
  );
  MUXCY   \Mcount_sclk_divider_cy<8>  (
    .CI(Mcount_sclk_divider_cy[7]),
    .DI(led3_OBUF_32),
    .S(\Mcount_sclk_divider_cy<8>_rt_105 ),
    .O(Mcount_sclk_divider_cy[8])
  );
  XORCY   \Mcount_sclk_divider_xor<8>  (
    .CI(Mcount_sclk_divider_cy[7]),
    .LI(\Mcount_sclk_divider_cy<8>_rt_105 ),
    .O(Result[8])
  );
  MUXCY   \Mcount_sclk_divider_cy<9>  (
    .CI(Mcount_sclk_divider_cy[8]),
    .DI(led3_OBUF_32),
    .S(\Mcount_sclk_divider_cy<9>_rt_106 ),
    .O(Mcount_sclk_divider_cy[9])
  );
  XORCY   \Mcount_sclk_divider_xor<9>  (
    .CI(Mcount_sclk_divider_cy[8]),
    .LI(\Mcount_sclk_divider_cy<9>_rt_106 ),
    .O(Result[9])
  );
  XORCY   \Mcount_sclk_divider_xor<10>  (
    .CI(Mcount_sclk_divider_cy[9]),
    .LI(\Mcount_sclk_divider_xor<10>_rt_107 ),
    .O(Result[10])
  );
  LUT2 #(
    .INIT ( 4'h4 ))
  _n01481 (
    .I0(rep_start_5),
    .I1(start_IBUF_1),
    .O(_n0148)
  );
  LUT4 #(
    .INIT ( 16'h0001 ))
  \ack<3>1  (
    .I0(acks[3]),
    .I1(acks[2]),
    .I2(acks[1]),
    .I3(acks[0]),
    .O(ack_OBUF_31)
  );
  LUT2 #(
    .INIT ( 4'hE ))
  Reset_OR_DriverANDClockEnable1 (
    .I0(rep_start_5),
    .I1(start_IBUF_1),
    .O(Reset_OR_DriverANDClockEnable)
  );
  LUT2 #(
    .INIT ( 4'h4 ))
  _n0180_inv2 (
    .I0(rep_start_5),
    .I1(_n0180_inv1_66),
    .O(_n0180_inv)
  );
  LUT4 #(
    .INIT ( 16'hFFFD ))
  \Mmux_sdat_stage[5]_MUX_102_o221  (
    .I0(stage[3]),
    .I1(stage[4]),
    .I2(stage[1]),
    .I3(stage[2]),
    .O(\Mmux_sdat_stage[5]_MUX_102_o22_67 )
  );
  LUT4 #(
    .INIT ( 16'hFBFF ))
  \stage[5]_acks[3]_select_19_OUT<1>21  (
    .I0(stage[5]),
    .I1(stage[4]),
    .I2(stage[2]),
    .I3(stage[1]),
    .O(\stage[5]_acks[3]_select_19_OUT<1>2 )
  );
  LUT3 #(
    .INIT ( 8'h01 ))
  \Mmux_clock_en_stage[5]_MUX_103_o1221  (
    .I0(stage[5]),
    .I1(stage[1]),
    .I2(stage[0]),
    .O(\Mmux_clock_en_stage[5]_MUX_103_o121 )
  );
  LUT6 #(
    .INIT ( 64'h6AAAAAAAAAAAAAAA ))
  \Mcount_stage_xor<5>11  (
    .I0(stage[5]),
    .I1(stage[0]),
    .I2(stage[1]),
    .I3(stage[2]),
    .I4(stage[3]),
    .I5(stage[4]),
    .O(\Result<5>1 )
  );
  LUT5 #(
    .INIT ( 32'h04030000 ))
  \Mmux_clock_en_stage[5]_MUX_103_o11  (
    .I0(rw_IBUF_2),
    .I1(stage[4]),
    .I2(stage[3]),
    .I3(stage[2]),
    .I4(\Mmux_clock_en_stage[5]_MUX_103_o121 ),
    .O(\Mmux_clock_en_stage[5]_MUX_103_o1 )
  );
  LUT6 #(
    .INIT ( 64'hFFEFFFFFF7E2F7FF ))
  \Mmux_clock_en_stage[5]_MUX_103_o12  (
    .I0(stage[3]),
    .I1(stage[4]),
    .I2(stage[5]),
    .I3(rw_IBUF_2),
    .I4(stage[0]),
    .I5(stage[1]),
    .O(\Mmux_clock_en_stage[5]_MUX_103_o11_71 )
  );
  LUT3 #(
    .INIT ( 8'h5D ))
  \Mmux_clock_en_stage[5]_MUX_103_o13  (
    .I0(stage[2]),
    .I1(stage[0]),
    .I2(stage[1]),
    .O(\Mmux_clock_en_stage[5]_MUX_103_o12_72 )
  );
  LUT5 #(
    .INIT ( 32'hFFA8FFAA ))
  \Mmux_clock_en_stage[5]_MUX_103_o14  (
    .I0(clock_en_21),
    .I1(\Mmux_clock_en_stage[5]_MUX_103_o12_72 ),
    .I2(\Mmux_clock_en_stage[5]_MUX_103_o11_71 ),
    .I3(\Mmux_clock_en_stage[5]_MUX_103_o1 ),
    .I4(_n0180_inv1_66),
    .O(\clock_en_stage[5]_MUX_103_o )
  );
  LUT5 #(
    .INIT ( 32'h80000000 ))
  \Mmux_sdat_stage[5]_MUX_102_o21  (
    .I0(sdat_4),
    .I1(stage[3]),
    .I2(stage[4]),
    .I3(stage[2]),
    .I4(stage[1]),
    .O(\Mmux_sdat_stage[5]_MUX_102_o2 )
  );
  LUT2 #(
    .INIT ( 4'hE ))
  \Mmux_sdat_stage[5]_MUX_102_o22  (
    .I0(stage[3]),
    .I1(stage[4]),
    .O(\Mmux_sdat_stage[5]_MUX_102_o21_74 )
  );
  LUT5 #(
    .INIT ( 32'h01000010 ))
  \Mmux_sdat_stage[5]_MUX_102_o23  (
    .I0(stage[4]),
    .I1(stage[0]),
    .I2(stage[3]),
    .I3(stage[1]),
    .I4(stage[2]),
    .O(\Mmux_sdat_stage[5]_MUX_102_o23_75 )
  );
  LUT6 #(
    .INIT ( 64'hCCCC8080FF88FF88 ))
  \Mmux_sdat_stage[5]_MUX_102_o24  (
    .I0(sdat_4),
    .I1(stage[5]),
    .I2(\Mmux_sdat_stage[5]_MUX_102_o21_74 ),
    .I3(\Mmux_sdat_stage[5]_MUX_102_o2 ),
    .I4(\Mmux_sdat_stage[5]_MUX_102_o23_75 ),
    .I5(midlow_GND_1_o_AND_39_o),
    .O(\Mmux_sdat_stage[5]_MUX_102_o24_76 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \Mmux_sdat_stage[5]_MUX_102_o26  (
    .I0(stage[3]),
    .I1(stage[4]),
    .O(\Mmux_sdat_stage[5]_MUX_102_o26_78 )
  );
  LUT5 #(
    .INIT ( 32'h00000020 ))
  \Mmux_sdat_stage[5]_MUX_102_o28  (
    .I0(N8),
    .I1(stage[2]),
    .I2(stage[0]),
    .I3(stage[4]),
    .I4(stage[1]),
    .O(\Mmux_sdat_stage[5]_MUX_102_o28_80 )
  );
  LUT6 #(
    .INIT ( 64'hFFFFFFFF55555150 ))
  \Mmux_sdat_stage[5]_MUX_102_o29  (
    .I0(stage[5]),
    .I1(stage[3]),
    .I2(\Mmux_sdat_stage[5]_MUX_102_o28_80 ),
    .I3(\Mmux_sdat_stage[5]_MUX_102_o25_77 ),
    .I4(\Mmux_sdat_stage[5]_MUX_102_o27_79 ),
    .I5(\Mmux_sdat_stage[5]_MUX_102_o24_76 ),
    .O(\sdat_stage[5]_MUX_102_o )
  );
  LUT3 #(
    .INIT ( 8'hDF ))
  \stage[5]_acks[3]_select_19_OUT<3>_SW0  (
    .I0(stage[5]),
    .I1(stage[3]),
    .I2(stage[2]),
    .O(N0)
  );
  LUT6 #(
    .INIT ( 64'hCCCCCCCCCCCACCCC ))
  \stage[5]_acks[3]_select_19_OUT<3>  (
    .I0(N8),
    .I1(acks[3]),
    .I2(stage[4]),
    .I3(stage[1]),
    .I4(stage[0]),
    .I5(N0),
    .O(\stage[5]_acks[3]_select_19_OUT<3>_26 )
  );
  LUT5 #(
    .INIT ( 32'h80808000 ))
  i2c_sclk_SW0 (
    .I0(sclk_divider[5]),
    .I1(sclk_divider[7]),
    .I2(sclk_divider[6]),
    .I3(sclk_divider[2]),
    .I4(sclk_divider[3]),
    .O(N2)
  );
  LUT6 #(
    .INIT ( 64'hFFFFFFFFFBBBBBBB ))
  i2c_sclk_58 (
    .I0(sclk_divider[10]),
    .I1(clock_en_21),
    .I2(sclk_divider[4]),
    .I3(sclk_divider[8]),
    .I4(N2),
    .I5(sclk_divider[9]),
    .O(i2c_sclk_OBUF_28)
  );
  LUT6 #(
    .INIT ( 64'hFFFFFFFFBFFFFFFF ))
  \sclk_divider[10]_GND_1_o_equal_7_o<10>_SW0  (
    .I0(sclk_divider[2]),
    .I1(sclk_divider[7]),
    .I2(sclk_divider[6]),
    .I3(sclk_divider[5]),
    .I4(sclk_divider[3]),
    .I5(sclk_divider[1]),
    .O(N4)
  );
  LUT6 #(
    .INIT ( 64'h0000000000001000 ))
  \sclk_divider[10]_GND_1_o_equal_7_o<10>  (
    .I0(sclk_divider[10]),
    .I1(sclk_divider[0]),
    .I2(sclk_divider[9]),
    .I3(sclk_divider[8]),
    .I4(sclk_divider[4]),
    .I5(N4),
    .O(\sclk_divider[10]_GND_1_o_equal_7_o )
  );
  LUT6 #(
    .INIT ( 64'hFFFFFFFFBFFFFFFF ))
  _n0180_inv1_SW0 (
    .I0(sclk_divider[5]),
    .I1(sclk_divider[2]),
    .I2(sclk_divider[1]),
    .I3(sclk_divider[7]),
    .I4(sclk_divider[6]),
    .I5(sclk_divider[3]),
    .O(N6)
  );
  LUT6 #(
    .INIT ( 64'h0000000000001000 ))
  _n0180_inv1 (
    .I0(sclk_divider[10]),
    .I1(sclk_divider[0]),
    .I2(sclk_divider[4]),
    .I3(sclk_divider[8]),
    .I4(sclk_divider[9]),
    .I5(N6),
    .O(_n0180_inv1_66)
  );
  LUT5 #(
    .INIT ( 32'hFFFFEAAA ))
  _n0213_inv13 (
    .I0(start_IBUF_1),
    .I1(stage[2]),
    .I2(_n0213_inv12_85),
    .I3(_n0180_inv1_66),
    .I4(\sclk_divider[10]_GND_1_o_equal_7_o ),
    .O(_n0213_inv1)
  );
  IBUF   start_IBUF (
    .I(start),
    .O(start_IBUF_1)
  );
  IBUF   rw_IBUF (
    .I(rw),
    .O(rw_IBUF_2)
  );
  IOBUF   i2c_sdat_IOBUF (
    .I(led3_OBUF_32),
    .T(sdat_4),
    .O(N8),
    .IO(i2c_sdat)
  );
  OBUF   i2c_sclk_OBUF (
    .I(i2c_sclk_OBUF_28),
    .O(i2c_sclk)
  );
  OBUF   led0_OBUF (
    .I(led3_OBUF_32),
    .O(led0)
  );
  OBUF   led1_OBUF (
    .I(led3_OBUF_32),
    .O(led1)
  );
  OBUF   led2_OBUF (
    .I(led3_OBUF_32),
    .O(led2)
  );
  OBUF   led3_OBUF (
    .I(led3_OBUF_32),
    .O(led3)
  );
  OBUF   done_OBUF (
    .I(done_OBUF_25),
    .O(done)
  );
  OBUF   ack_OBUF (
    .I(ack_OBUF_31),
    .O(ack)
  );
  FDR   rep_start (
    .C(clk_BUFGP_0),
    .D(rep_start_glue_set_97),
    .R(rep_start_5),
    .Q(rep_start_5)
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_sclk_divider_cy<1>_rt  (
    .I0(sclk_divider[1]),
    .O(\Mcount_sclk_divider_cy<1>_rt_98 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_sclk_divider_cy<2>_rt  (
    .I0(sclk_divider[2]),
    .O(\Mcount_sclk_divider_cy<2>_rt_99 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_sclk_divider_cy<3>_rt  (
    .I0(sclk_divider[3]),
    .O(\Mcount_sclk_divider_cy<3>_rt_100 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_sclk_divider_cy<4>_rt  (
    .I0(sclk_divider[4]),
    .O(\Mcount_sclk_divider_cy<4>_rt_101 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_sclk_divider_cy<5>_rt  (
    .I0(sclk_divider[5]),
    .O(\Mcount_sclk_divider_cy<5>_rt_102 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_sclk_divider_cy<6>_rt  (
    .I0(sclk_divider[6]),
    .O(\Mcount_sclk_divider_cy<6>_rt_103 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_sclk_divider_cy<7>_rt  (
    .I0(sclk_divider[7]),
    .O(\Mcount_sclk_divider_cy<7>_rt_104 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_sclk_divider_cy<8>_rt  (
    .I0(sclk_divider[8]),
    .O(\Mcount_sclk_divider_cy<8>_rt_105 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_sclk_divider_cy<9>_rt  (
    .I0(sclk_divider[9]),
    .O(\Mcount_sclk_divider_cy<9>_rt_106 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \Mcount_sclk_divider_xor<10>_rt  (
    .I0(sclk_divider[10]),
    .O(\Mcount_sclk_divider_xor<10>_rt_107 )
  );
  LUT6 #(
    .INIT ( 64'hCCCCCDCCCCCCCCCC ))
  rep_start_glue_set (
    .I0(start_IBUF_1),
    .I1(rep_start_5),
    .I2(stage[3]),
    .I3(stage[0]),
    .I4(\stage[5]_acks[3]_select_19_OUT<1>2 ),
    .I5(midlow_GND_1_o_AND_39_o),
    .O(rep_start_glue_set_97)
  );
  LUT2 #(
    .INIT ( 4'hE ))
  _n0180_inv1_SW1 (
    .I0(rw_IBUF_2),
    .I1(sclk_divider[0]),
    .O(N10)
  );
  LUT6 #(
    .INIT ( 64'h0000000000000400 ))
  midlow_GND_1_o_AND_39_o1 (
    .I0(sclk_divider[9]),
    .I1(sclk_divider[8]),
    .I2(sclk_divider[10]),
    .I3(sclk_divider[4]),
    .I4(N10),
    .I5(N6),
    .O(midlow_GND_1_o_AND_39_o)
  );
  LUT5 #(
    .INIT ( 32'hFFFCFC4F ))
  midlow_GND_1_o_AND_39_o1_SW0 (
    .I0(N8),
    .I1(stage[4]),
    .I2(stage[1]),
    .I3(stage[2]),
    .I4(stage[0]),
    .O(N12)
  );
  LUT6 #(
    .INIT ( 64'h0000FFFB0004FFFF ))
  \Mmux_sdat_stage[5]_MUX_102_o25  (
    .I0(sclk_divider[9]),
    .I1(sclk_divider[8]),
    .I2(N10),
    .I3(N6),
    .I4(N12),
    .I5(N13),
    .O(\Mmux_sdat_stage[5]_MUX_102_o25_77 )
  );
  LUT4 #(
    .INIT ( 16'hD3FF ))
  midlow_GND_1_o_AND_39_o1_SW3 (
    .I0(N8),
    .I1(stage[2]),
    .I2(stage[1]),
    .I3(stage[0]),
    .O(N16)
  );
  LUT6 #(
    .INIT ( 64'h222222202222222A ))
  \Mmux_sdat_stage[5]_MUX_102_o27  (
    .I0(\Mmux_sdat_stage[5]_MUX_102_o26_78 ),
    .I1(N16),
    .I2(N6),
    .I3(N10),
    .I4(sclk_divider[9]),
    .I5(N15),
    .O(\Mmux_sdat_stage[5]_MUX_102_o27_79 )
  );
  LUT6 #(
    .INIT ( 64'h0004000000002000 ))
  _n0213_inv12 (
    .I0(rw_IBUF_2),
    .I1(stage[5]),
    .I2(stage[3]),
    .I3(stage[4]),
    .I4(stage[1]),
    .I5(stage[0]),
    .O(_n0213_inv12_85)
  );
  MUXF7   midlow_GND_1_o_AND_39_o1_SW1 (
    .I0(N18),
    .I1(N19),
    .S(stage[1]),
    .O(N13)
  );
  LUT5 #(
    .INIT ( 32'hBFF0F0FF ))
  midlow_GND_1_o_AND_39_o1_SW1_F (
    .I0(sclk_divider[10]),
    .I1(sclk_divider[4]),
    .I2(stage[4]),
    .I3(stage[2]),
    .I4(stage[0]),
    .O(N18)
  );
  LUT6 #(
    .INIT ( 64'hFFFFCFFFCFFF5500 ))
  midlow_GND_1_o_AND_39_o1_SW1_G (
    .I0(N8),
    .I1(sclk_divider[10]),
    .I2(sclk_divider[4]),
    .I3(stage[4]),
    .I4(stage[2]),
    .I5(stage[0]),
    .O(N19)
  );
  MUXF7   midlow_GND_1_o_AND_39_o1_SW2 (
    .I0(N20),
    .I1(N21),
    .S(stage[1]),
    .O(N15)
  );
  LUT6 #(
    .INIT ( 64'h0400FFFFF3FFF3FF ))
  midlow_GND_1_o_AND_39_o1_SW2_F (
    .I0(N8),
    .I1(sclk_divider[8]),
    .I2(sclk_divider[10]),
    .I3(sclk_divider[4]),
    .I4(stage[2]),
    .I5(stage[0]),
    .O(N20)
  );
  LUT6 #(
    .INIT ( 64'hFFFF5D55FFFFFFFF ))
  midlow_GND_1_o_AND_39_o1_SW2_G (
    .I0(N8),
    .I1(sclk_divider[8]),
    .I2(sclk_divider[10]),
    .I3(sclk_divider[4]),
    .I4(stage[2]),
    .I5(stage[0]),
    .O(N21)
  );
  LUT6 #(
    .INIT ( 64'h0000000000001000 ))
  \done<5>1  (
    .I0(stage[0]),
    .I1(stage[4]),
    .I2(stage[3]),
    .I3(stage[5]),
    .I4(stage[1]),
    .I5(stage[2]),
    .O(done_OBUF_25)
  );
  FD   sclk_divider_1 (
    .C(clk_BUFGP_0),
    .D(sclk_divider_1_rstpot_117),
    .Q(sclk_divider[1])
  );
  FD   sclk_divider_2 (
    .C(clk_BUFGP_0),
    .D(sclk_divider_2_rstpot_118),
    .Q(sclk_divider[2])
  );
  FD   sclk_divider_3 (
    .C(clk_BUFGP_0),
    .D(sclk_divider_3_rstpot_119),
    .Q(sclk_divider[3])
  );
  FD   sclk_divider_6 (
    .C(clk_BUFGP_0),
    .D(sclk_divider_6_rstpot_120),
    .Q(sclk_divider[6])
  );
  FD   sclk_divider_4 (
    .C(clk_BUFGP_0),
    .D(sclk_divider_4_rstpot_121),
    .Q(sclk_divider[4])
  );
  FD   sclk_divider_5 (
    .C(clk_BUFGP_0),
    .D(sclk_divider_5_rstpot_122),
    .Q(sclk_divider[5])
  );
  FD   sclk_divider_7 (
    .C(clk_BUFGP_0),
    .D(sclk_divider_7_rstpot_123),
    .Q(sclk_divider[7])
  );
  FD   sclk_divider_8 (
    .C(clk_BUFGP_0),
    .D(sclk_divider_8_rstpot_124),
    .Q(sclk_divider[8])
  );
  FD   sclk_divider_9 (
    .C(clk_BUFGP_0),
    .D(sclk_divider_9_rstpot_125),
    .Q(sclk_divider[9])
  );
  FD   sclk_divider_0 (
    .C(clk_BUFGP_0),
    .D(sclk_divider_0_rstpot_126),
    .Q(sclk_divider[0])
  );
  FD   sclk_divider_10 (
    .C(clk_BUFGP_0),
    .D(sclk_divider_10_rstpot_127),
    .Q(sclk_divider[10])
  );
  LUT6 #(
    .INIT ( 64'hF0C000C0F5C400C4 ))
  sclk_divider_5_rstpot (
    .I0(start_IBUF_1),
    .I1(sclk_divider[5]),
    .I2(rep_start_5),
    .I3(rep_start_inv),
    .I4(Result[5]),
    .I5(\sclk_divider[10]_GND_1_o_equal_7_o ),
    .O(sclk_divider_5_rstpot_122)
  );
  LUT6 #(
    .INIT ( 64'hF0C000C0F5C400C4 ))
  sclk_divider_6_rstpot (
    .I0(start_IBUF_1),
    .I1(sclk_divider[6]),
    .I2(rep_start_5),
    .I3(rep_start_inv),
    .I4(Result[6]),
    .I5(\sclk_divider[10]_GND_1_o_equal_7_o ),
    .O(sclk_divider_6_rstpot_120)
  );
  LUT6 #(
    .INIT ( 64'hF0C000C0F5C400C4 ))
  sclk_divider_7_rstpot (
    .I0(start_IBUF_1),
    .I1(sclk_divider[7]),
    .I2(rep_start_5),
    .I3(rep_start_inv),
    .I4(Result[7]),
    .I5(\sclk_divider[10]_GND_1_o_equal_7_o ),
    .O(sclk_divider_7_rstpot_123)
  );
  LUT6 #(
    .INIT ( 64'hF0C000C0F5C400C4 ))
  sclk_divider_8_rstpot (
    .I0(start_IBUF_1),
    .I1(sclk_divider[8]),
    .I2(rep_start_5),
    .I3(rep_start_inv),
    .I4(Result[8]),
    .I5(\sclk_divider[10]_GND_1_o_equal_7_o ),
    .O(sclk_divider_8_rstpot_124)
  );
  LUT6 #(
    .INIT ( 64'hF0C0F5C400C000C4 ))
  sclk_divider_9_rstpot (
    .I0(start_IBUF_1),
    .I1(sclk_divider[9]),
    .I2(rep_start_5),
    .I3(rep_start_inv),
    .I4(\sclk_divider[10]_GND_1_o_equal_7_o ),
    .I5(Result[9]),
    .O(sclk_divider_9_rstpot_125)
  );
  LUT6 #(
    .INIT ( 64'hF0C0F5C400C000C4 ))
  sclk_divider_10_rstpot (
    .I0(start_IBUF_1),
    .I1(sclk_divider[10]),
    .I2(rep_start_5),
    .I3(rep_start_inv),
    .I4(\sclk_divider[10]_GND_1_o_equal_7_o ),
    .I5(Result[10]),
    .O(sclk_divider_10_rstpot_127)
  );
  LUT3 #(
    .INIT ( 8'hE2 ))
  acks_3_dpot (
    .I0(acks[3]),
    .I1(_n01671_rstpot),
    .I2(\stage[5]_acks[3]_select_19_OUT<3>_26 ),
    .O(acks_3_dpot_138)
  );
  LUT6 #(
    .INIT ( 64'hAAAAEAAAAAAA2AAA ))
  acks_2_dpot (
    .I0(acks[2]),
    .I1(_n01671_rstpot),
    .I2(stage[3]),
    .I3(stage[0]),
    .I4(\stage[5]_acks[3]_select_19_OUT<1>2 ),
    .I5(N8),
    .O(acks_2_dpot_137)
  );
  LUT6 #(
    .INIT ( 64'hAAAABAAAAAAA8AAA ))
  acks_0_dpot (
    .I0(acks[0]),
    .I1(\Mmux_sdat_stage[5]_MUX_102_o22_67 ),
    .I2(_n01671_rstpot),
    .I3(stage[0]),
    .I4(stage[5]),
    .I5(N8),
    .O(acks_0_dpot_135)
  );
  LUT6 #(
    .INIT ( 64'hAAAAAABAAAAAAA8A ))
  acks_1_dpot (
    .I0(acks[1]),
    .I1(stage[0]),
    .I2(_n01671_rstpot),
    .I3(stage[3]),
    .I4(\stage[5]_acks[3]_select_19_OUT<1>2 ),
    .I5(N8),
    .O(acks_1_dpot_136)
  );
  LUT5 #(
    .INIT ( 32'hBBBB888A ))
  stage_5_dpot (
    .I0(stage[5]),
    .I1(rep_start_5),
    .I2(\Mmux_sdat_stage[5]_MUX_102_o22_67 ),
    .I3(stage[0]),
    .I4(\Result<5>1 ),
    .O(stage_5_dpot_133)
  );
  LUT5 #(
    .INIT ( 32'hC0C0C5C0 ))
  sclk_divider_1_rstpot (
    .I0(start_IBUF_1),
    .I1(sclk_divider[1]),
    .I2(rep_start_5),
    .I3(Result[1]),
    .I4(\sclk_divider[10]_GND_1_o_equal_7_o ),
    .O(sclk_divider_1_rstpot_117)
  );
  LUT5 #(
    .INIT ( 32'hC0C0C5C0 ))
  sclk_divider_2_rstpot (
    .I0(start_IBUF_1),
    .I1(sclk_divider[2]),
    .I2(rep_start_5),
    .I3(Result[2]),
    .I4(\sclk_divider[10]_GND_1_o_equal_7_o ),
    .O(sclk_divider_2_rstpot_118)
  );
  LUT5 #(
    .INIT ( 32'hC0C0C5C0 ))
  sclk_divider_3_rstpot (
    .I0(start_IBUF_1),
    .I1(sclk_divider[3]),
    .I2(rep_start_5),
    .I3(Result[3]),
    .I4(\sclk_divider[10]_GND_1_o_equal_7_o ),
    .O(sclk_divider_3_rstpot_119)
  );
  LUT5 #(
    .INIT ( 32'hC0C0C5C0 ))
  sclk_divider_0_rstpot (
    .I0(start_IBUF_1),
    .I1(sclk_divider[0]),
    .I2(rep_start_5),
    .I3(Result[0]),
    .I4(\sclk_divider[10]_GND_1_o_equal_7_o ),
    .O(sclk_divider_0_rstpot_126)
  );
  LUT5 #(
    .INIT ( 32'hC0C0C5C0 ))
  sclk_divider_4_rstpot (
    .I0(start_IBUF_1),
    .I1(sclk_divider[4]),
    .I2(rep_start_5),
    .I3(Result[4]),
    .I4(\sclk_divider[10]_GND_1_o_equal_7_o ),
    .O(sclk_divider_4_rstpot_121)
  );
  LUT6 #(
    .INIT ( 64'hAAAA6AAAAAAAAAAA ))
  stage_4_dpot (
    .I0(stage[4]),
    .I1(stage[2]),
    .I2(stage[0]),
    .I3(stage[1]),
    .I4(rep_start_5),
    .I5(stage[3]),
    .O(stage_4_dpot_132)
  );
  LUT5 #(
    .INIT ( 32'hAA6AAAAA ))
  stage_3_dpot (
    .I0(stage[3]),
    .I1(stage[1]),
    .I2(stage[0]),
    .I3(rep_start_5),
    .I4(stage[2]),
    .O(stage_3_dpot_131)
  );
  LUT4 #(
    .INIT ( 16'hA6AA ))
  stage_2_dpot (
    .I0(stage[2]),
    .I1(stage[0]),
    .I2(rep_start_5),
    .I3(stage[1]),
    .O(stage_2_dpot_130)
  );
  LUT3 #(
    .INIT ( 8'hA6 ))
  stage_1_dpot (
    .I0(stage[1]),
    .I1(stage[0]),
    .I2(rep_start_5),
    .O(stage_1_dpot_129)
  );
  LUT4 #(
    .INIT ( 16'h9899 ))
  stage_0_dpot (
    .I0(stage[0]),
    .I1(rep_start_5),
    .I2(\Mmux_sdat_stage[5]_MUX_102_o22_67 ),
    .I3(stage[5]),
    .O(stage_0_dpot_128)
  );
  BUFGP   clk_BUFGP (
    .I(clk),
    .O(clk_BUFGP_0)
  );
  INV   \Mcount_sclk_divider_lut<0>_INV_0  (
    .I(sclk_divider[0]),
    .O(Mcount_sclk_divider_lut[0])
  );
  INV   rep_start_inv1_INV_0 (
    .I(rep_start_5),
    .O(rep_start_inv)
  );
  INV   _n01671_rstpot_INV_0 (
    .I(rep_start_5),
    .O(_n01671_rstpot)
  );
endmodule


`ifndef GLBL
`define GLBL

`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;

    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule

`endif

