
#include "soc-hw.h"

int main()
{
//uart_putchar(0x12);
//send_time(0xAA55E8);
//}
//uart_putstr("**MSAS Proyect alpha 1 system test**\n");
//msleep(1000);
//	uint32_t *systime;	
	//set_time(0x00,0x10,0x09,0x02,0x24,0x05,0x16);
	
/*	while(1){
	*systime = get_time();
	msleep(1000);
	//i2c_read(0x68, 0x00);
	uart_putint(0x00);
	uart_putint(systime[0]);
        uart_putint(systime[1]);
	uart_putint(systime[2]);
        uart_putint(systime[3]);
	uart_putint(systime[4]);
        uart_putint(systime[5]);
        uart_putint(systime[6]);
}*/
//send_time('0x550068');
//msleep(5000);
//uart_send_cmd(0x0F,0x01,0x01,0xFE,0xEA); //Reproducir pista 001, carpeta 01
//msleep(1500);
//uart_send_cmd(0x06,0x00,0x14,0xFE,0xE1); //ajustar volumen a 20
//while(1){
//	uart_putstr("TICK");
//	msleep(10000);

lcdInit();
msleep(2000);
char temp_char;
//writeCharlcd('a');
uart_putstr("Type whatever you want to see on screen\n\r");
while (1){
	temp_char = uart_getchar();
	writeCharlcd(temp_char);
}

//writeStringlcd("MSAS Project Demo ver 0.1");
return 0;
}


//---------------------------------------------------------------------------
// LCD Functions
//---------------------------------------------------------------------------

void writeCharlcd (char letter) 
{
  char highnib;
  char lownib;
  highnib = letter&0xF0;
  lownib = letter&0x0F;

     i2c_write(0x27, 0x00, highnib|0b00001001);
     i2c_write(0x27, 0x00, highnib|0b00001101);
     i2c_write(0x27, 0x00, highnib|0b00001001);  

     i2c_write(0x27, 0x00, (lownib<<4)|0b00001001);
     i2c_write(0x27, 0x00, (lownib<<4)|0b00001101);
     i2c_write(0x27, 0x00, (lownib<<4)|0b00001001);
}




void writeCommandlcd (char command) 
{
  char highnib;
  char lownib;
  highnib = command&0xF0;
  lownib = command&0x0F;

     i2c_write(0x27, 0x00, highnib|0b00001000);
     i2c_write(0x27, 0x00, highnib|0b00001100);
     i2c_write(0x27, 0x00, highnib|0b00001000);  

     i2c_write(0x27, 0x00, (lownib<<4)|0b00001000);
     i2c_write(0x27, 0x00, (lownib<<4)|0b00001100);
     i2c_write(0x27, 0x00, (lownib<<4)|0b00001000);
}




void writeStringlcd (char *str) {
	char *c = str;
	while(*c) {
		writeCharlcd(*c);
		c++;
	}
}

// LCD Commands------------------------------------------

// LCD_I2C CONFIG
// DB7 DB6 DB5 DB4 CTRST EN RW RS

void clearDisplay() 
{
   writeCommandlcd(0b00000001);
}

void returnHome()
{
   writeCommandlcd(0b00000010);
   msleep(2);
}

// I/D = 1, S=0
void entryModeSet2()
{  
  
   writeCommandlcd(0b00000110);
   msleep(1);
}

// I/D = 1, S=1
void entryModeSet()
{  
   writeCommandlcd(0b00000111);
   msleep(1);
}


// I/D = 0, S=0
void entryModeSet3()
{  
   writeCommandlcd(0b00000100);
   msleep(1);
}

// I/D = 0, S=1
void entryModeSet4()
{  
   writeCommandlcd(0b00000101);
   msleep(1);
}


void displayOff()
{  
   writeCommandlcd(0b00001000);
   msleep(1);
}

// D=1, C=1, B=1
void displayOn()
{  

   writeCommandlcd(0b00001111);
   msleep(1);
}


// S/C = 0, R/L = 1
void cursorShiftRight()
{  
   writeCommandlcd(0b00010100);
   msleep(1);
}



// S/C = 0, R/L = 0
void cursorShiftLeft()
{  
   writeCommandlcd(0b00010000);
   msleep(1);
}


// S/C = 1, R/L = 1
void displayShiftRight()
{   

   writeCommandlcd(0b00011100);
   msleep(1);
}


// S/C = 1, R/L = 0
void displayShiftLeft()
{  
   writeCommandlcd(0b00011000);
   msleep(1);
}



// D/L = 0, N = 1, F = 0
//4-Bit mode, 2 lines, 5x8 dots
void functionSet()
{  

   writeCommandlcd(0b00101000);
   msleep(1);
}




void displayAddress(uint8_t col, uint8_t row){
	int row_offsets[] = { 0x00, 0x40, 0x14, 0x54 };
	if ( row > 2 ) {
		row = 2-1;    // we count rows starting w/0
	}
        writeCommandlcd (0x80|(col + row_offsets[row]));
}

void lcdInit ()
{  //1
   // LCD_I2C CONFIG
   // DB7 DB6 DB5 DB4 CTRST EN RW RS
   msleep(50);
   i2c_write(0x27, 0x00, 0b00111000);
   i2c_write(0x27, 0x00, 0b00111100);
   i2c_write(0x27, 0x00, 0b00111000);
   msleep(5);
   
   //2
   i2c_write(0x27, 0x00, 0b00111000);
   i2c_write(0x27, 0x00, 0b00111100);
   i2c_write(0x27, 0x00, 0b00111000);
   msleep(5);
   //3
   i2c_write(0x27, 0x00, 0b00111000);
   i2c_write(0x27, 0x00, 0b00111100);
   i2c_write(0x27, 0x00, 0b00111000);
   msleep(1);
   //5
   i2c_write(0x27, 0x00, 0b00101000);
   i2c_write(0x27, 0x00, 0b00101100);
   i2c_write(0x27, 0x00, 0b00101000);
   msleep(1);
   //6
   i2c_write(0x27, 0x00, 0b00101000);
   i2c_write(0x27, 0x00, 0b00101100);
   i2c_write(0x27, 0x00, 0b00101000);
   msleep(1);
   //7
   i2c_write(0x27, 0x00, 0b10001000);
   i2c_write(0x27, 0x00, 0b10001100);
   i2c_write(0x27, 0x00, 0b10001000);
   msleep(1);
   //8
   i2c_write(0x27, 0x00, 0b00001000);
   i2c_write(0x27, 0x00, 0b00001100);
   i2c_write(0x27, 0x00, 0b00001000);
   msleep(1);
   //9
   i2c_write(0x27, 0x00, 0b10001000);
   i2c_write(0x27, 0x00, 0b10001100);
   i2c_write(0x27, 0x00, 0b10001000);
   msleep(1);
   //10
   i2c_write(0x27, 0x00, 0b00001000);
   i2c_write(0x27, 0x00, 0b00001100);
   i2c_write(0x27, 0x00, 0b00001000);
   msleep(2);
   //11
   i2c_write(0x27, 0x00, 0b11111000);
   i2c_write(0x27, 0x00, 0b11111100);
   i2c_write(0x27, 0x00, 0b11111000);
   msleep(1);
   //12
   i2c_write(0x27, 0x00, 0b00001000);
   i2c_write(0x27, 0x00, 0b00001100);
   i2c_write(0x27, 0x00, 0b00001000);
   msleep(1);
   //13
   i2c_write(0x27, 0x00, 0b01101000);
   i2c_write(0x27, 0x00, 0b01101100);
   i2c_write(0x27, 0x00, 0b01101000);
   msleep(2);

}
