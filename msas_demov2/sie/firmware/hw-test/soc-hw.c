#include "soc-hw.h"

uart_t  *uart0  = (uart_t *)   0x10000000;
timer_t *timer0 = (timer_t *)  0x30000000;
i2c_t   *i2c0   = (i2c_t *)    0x20000000;
gpio_t  *gpio0  = (gpio_t *)   0x40000000;


isr_ptr_t isr_table[32];

void prueba()
{
	   uart0->rxtx=30;
	   timer0->tcr0 = 0xAA;
	   gpio0->ctrl=0x55;
}
void tic_isr();
/***************************************************************************
 * IRQ handling
 */
void isr_null()
{
}

void irq_handler(uint32_t pending)
{
	int i;

	for(i=0; i<32; i++) {
		if (pending & 0x01) (*isr_table[i])();
		pending >>= 1;
	}
}

void isr_init()
{
	int i;
	for(i=0; i<32; i++)
		isr_table[i] = &isr_null;
}

void isr_register(int irq, isr_ptr_t isr)
{
	isr_table[irq] = isr;
}

void isr_unregister(int irq)
{
	isr_table[irq] = &isr_null;
}

/***************************************************************************
 * TIMER Functions
 */
void msleep(uint32_t msec)
{
	uint32_t tcr;

	// Use timer0.1
	timer0->compare1 = (FCPU/1000)*msec;
	timer0->counter1 = 0;
	timer0->tcr1 = TIMER_EN;

	do {
		//halt();
 		tcr = timer0->tcr1;
 	} while ( ! (tcr & TIMER_TRIG) );
}

void nsleep(uint32_t nsec)
{
	uint32_t tcr;

	// Use timer0.1
	timer0->compare1 = (FCPU/1000000)*nsec;
	timer0->counter1 = 0;
	timer0->tcr1 = TIMER_EN;

	do {
		//halt();
 		tcr = timer0->tcr1;
 	} while ( ! (tcr & TIMER_TRIG) );
}


uint32_t tic_msec;

void tic_isr()
{
	tic_msec++;
	timer0->tcr0     = TIMER_EN | TIMER_AR | TIMER_IRQEN;
}

void tic_init()
{
	tic_msec = 0;

	// Setup timer0.0
	timer0->compare0 = (FCPU/10000);
	timer0->counter0 = 0;
	timer0->tcr0     = TIMER_EN | TIMER_AR | TIMER_IRQEN;

	isr_register(1, &tic_isr);
}


/***************************************************************************
 * UART Functions
 */
void uart_init()
{
	//uart0->ier = 0x00;  // Interrupt Enable Register
	//uart0->lcr = 0x03;  // Line Control Register:    8N1
	//uart0->mcr = 0x00;  // Modem Control Register

	// Setup Divisor register (Fclk / Baud)
	//uart0->div = (FCPU/(9600*16));
}

char uart_getchar()
{   
	while (! (uart0->ucr & UART_DR)) ;
	return uart0->rxtx;
}

void uart_play()
{
	uint8_t i = 0;
	uint32_t play_cmd[10] = {0x7E,0xFF,0x06,0x0F,0x00,0x01,0x01,0xFE,0xEA,0xEF};
	for(i = 0; i < 10; i++){	
		while (uart0->ucr & UART_BUSY);
		uart0->rxtx = play_cmd[i];
		msleep(100);
	}
}

void uart_send_cmd(uint32_t cmd, uint32_t par1, uint32_t par2, uint32_t chksum1, uint32_t chksum2){

	uint8_t i = 0;										//7E es el byte de inicio y EF es el byte de finalización
	uint32_t cmd_buffer[10] = {0x7E,0xFF,0x06,cmd,0x00,par1,par2,chksum1,chksum2,0xEF};	//FF indica la versión, 06 indica el número de bytes sin	
	for(i = 0; i < 10; i++){	                                			//contar el byte de inicio, finalización y checksum
		while (uart0->ucr & UART_BUSY);							//El cuarto byte corresponde al comando, el quinto a la 
		uart0->rxtx = cmd_buffer[i];							//activación de realimentación (feedback), sexto y séptimo
		//msleep(100);									//son los parámetros del comando, octavo y noveno son los
	}											//bytes de checksum 
}


void uart_putchar(char c)
{
	while (uart0->ucr & UART_BUSY) ;
	uart0->rxtx = c;
}
void uart_putstr(char *str)
{
	char *c = str;
	while(*c) {
		uart_putchar(*c);
		c++;
	}
}

void uart_putint(uint32_t number)
{
	while (uart0->ucr & UART_BUSY) ;
	uart0->rxtx = number;
}

void uart_volume25(){
	uart_send_cmd(0x06,0x00,0x19,0xFE,0xDC);
	}

void uart_track1(){
	uart_volume25();
	msleep(10);
	uart_send_cmd(0x0F,0x01,0x01,0xFE,0xEA);
	}

void uart_track2(){
	uart_volume25();
	msleep(10);
	uart_send_cmd(0x0F,0x01,0x02,0xFE,0xE9);
	}

void uart_track3(){
	uart_volume25();
	msleep(10);
	uart_send_cmd(0x0F,0x01,0x03,0xFE,0xE8);
	}

void uart_track4(){
	uart_volume25();
	msleep(10);
	uart_send_cmd(0x0F,0x01,0x04,0xFE,0xE7);
	}

void uart_track5(){
	uart_volume25();
	msleep(10);
	uart_send_cmd(0x0F,0x01,0x05,0xFE,0xE6);
	}

void uart_pause(){
	uart_send_cmd(0x0E,0x00,0x00,0xFE,0xED);
	}

void uart_continue(){
	uart_volume25();
	msleep(10);
	uart_send_cmd(0x0D,0x00,0x00,0xFE,0xEE);
	}

void uart_reset(){
	uart_send_cmd(0x0C,0x00,0x00,0xFE,0xEF);
	}

/***************************************************************************
 * I2C Functions
 */


uint32_t i2c_read(uint32_t slave_addr, uint32_t per_addr)
{
		
	while(!(i2c0->scr & I2C_DR));
	i2c0->sdat = (slave_addr | per_addr<<8);
	return i2c0->sdat;

}

void i2c_write(uint32_t slave_addr, uint32_t per_addr, uint32_t data){
	
	while(!(i2c0->scr & I2C_DR));
	i2c0->sdat = (slave_addr | per_addr<<8 |data<<16);

}

uint32_t * get_time(){
	
	static uint32_t systime[7] = {0,0,0,0,0,0,0}; //inicializar el arreglo del tiempo, static para ver el arreglo por fuera de la función
	
	systime[0]  = i2c_read(0x68, 0x00); //segundos
	systime[1]  = i2c_read(0x68, 0x01); //minutos
	systime[2]  = i2c_read(0x68, 0x02); //hora
	systime[3]  = i2c_read(0x68, 0x03); //día de la semana
	systime[4]  = i2c_read(0x68, 0x04); //día 
	systime[5]  = i2c_read(0x68, 0x05); //mes
	systime[6]  = i2c_read(0x68, 0x06); //año

	return systime;
}

void set_time(uint32_t seconds, uint32_t minutes, uint32_t hour, uint32_t wk_day, uint32_t day, uint32_t month, uint32_t year){

	i2c_write(0x68, 0x00, seconds);	//0x68 es la dirección en hex. del RTC, 0x00 es la dirección del registro
	i2c_write(0x68, 0x01, minutes);	
	i2c_write(0x68, 0x02, hour);	
	i2c_write(0x68, 0x03, wk_day);	
	i2c_write(0x68, 0x04, day);	
	i2c_write(0x68, 0x05, month);	
	i2c_write(0x68, 0x06, year);	
	
}

/***************************************************************************
 * GPIO Functions
 */

uint32_t get_keypad(){
	return gpio0->keypad;
}

void move_servo0(uint32_t pulse_width){
	gpio0->servo0 = pulse_width;
}

void move_servo1(uint32_t pulse_width){
	gpio0->servo1 = pulse_width;
}


