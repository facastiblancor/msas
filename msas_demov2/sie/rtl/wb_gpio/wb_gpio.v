//---------------------------------------------------------------------------
// Wishbone General Pupose IO Component
//
//     0x00	
//     0x10     gpio_in    (read-only)
//     0x14     gpio_out   (read/write)
//     0x18     gpio_oe    (read/write)
//
//---------------------------------------------------------------------------

module wb_gpio (
	input              clk,
	input              reset,
	// Wishbone interface
	input              wb_stb_i,
	input              wb_cyc_i,
	output             wb_ack_o,
	input              wb_we_i,
	input       [31:0] wb_adr_i,
	input        [3:0] wb_sel_i,
	input       [31:0] wb_dat_i,
	output reg  [31:0] wb_dat_o,
	//
	output             intr,  //used for enabling gpio interrupts
	
	//MODIFICACION PARA EL PROYECTO

	// IO Wires
	//input       [31:0] gpio_keypad,                              //DEFINE LAS ENTRADAS Y SALIDAS DE LA FPGA
	output             PWM_servo0,
        output             PWM_servo1,
	input  [3:0]	   rows,
	output [3:0]	   column	
);

//---------------------------------------------------------------------------
// 
//---------------------------------------------------------------------------

wire [31:0] gpiocr = 32'b0;

//los pines de entrada/Salida del GPIO son ahora wires:

reg [31:0] pwm0 = 1'b0;
reg [31:0] pwm1 = 1'b0;
wire [7:0] data;
wire [7:0] value;

// Wishbone
reg  ack;
assign wb_ack_o = wb_stb_i & wb_cyc_i & ack;

wire wb_rd = wb_stb_i & wb_cyc_i & ~wb_we_i;
wire wb_wr = wb_stb_i & wb_cyc_i &  wb_we_i;

always @(posedge clk)
begin
	if (reset) begin
		ack      <= 1'b0;
		//gpio_out <= 'b0;
	end else begin
		// Handle WISHBONE access
		ack    <= 1'b0;

		if (wb_rd & ~ack) begin           // read cycle
			ack <= 1'b1;


		//DEFINE UN CASE PARA LEER O ESCRIBIR LOS DATOS

			case (wb_adr_i[3:2])
			2'b00: wb_dat_o <= gpiocr;
			2'b11: wb_dat_o <= data;
			default: wb_dat_o <= 32'b0;
			endcase
		end else if (wb_wr & ~ack ) begin // write cycle
			ack <= 1'b1;

			case (wb_adr_i[3:2])
			2'b01: pwm0 <= wb_dat_i;
			2'b10: pwm1 <= wb_dat_i;
			default: wb_dat_o <= 32'b0;
			endcase
		end
	end
end

// ************* ACTUAL GPIO FUNCTION **********************
// *********************************************************

// modulo creado: Nombre "interfaz".
//gpio interfaz (  
//        .switches( switches ),
//	.leds    ( leds     ),
//	.in      ( gpio_out),
//	.in2     ( gpio_oe ),
//	.out     ( gpio_in )
//);	

motor servo0(
	.clk(       clk       ),
	.rst(       reset     ),
	.PWM_width(    pwm0   ),  // conteo del ancho de pulso calculado por el procesador
	.PWM(       PWM_servo0)   // SALIDA en PWM hacia el servo 0.
);

motor servo1(
	.clk(       clk       ),
	.rst(       reset     ),
	.PWM_width(   pwm1    ),  // conteo del ancho de pulso calculado por el procesador
	.PWM(       PWM_servo1)   // SALIDA en PWM hacia el servo 1.
);

Clock_ Clock_0(
	.clk_in(       clk    ),
	.clk_out(    clk_div  )
);

keypad keypad0(
	.clk_out(     clk_div ),
	.column(      column  ),
	.files(	      rows    ),
	.value(	      value   )	
);

Register Register0(
	.value(	       value  ),
	.clk_out(      clk_div),	
	.data(		data  )

);
	
	

endmodule
